import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { map, switchMap, take, tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { LoginService } from '../login/login.service';
import { Restaurante } from '../restaurantes/restaurante.model';
import { Reservacion } from './reservacion.model';
@Injectable({
 providedIn: 'root'
})
export class ReservacionService {
 private _reservaciones = new BehaviorSubject<Reservacion[]>([]);
 usuarioId = null;

 get reservaciones(){
 return this._reservaciones.asObservable();
 }

 fetchReservaciones(){
 let url = environment.firebaseUrl + 'reservaciones.json?orderBy="usuarioId"&equalTo="'+ this.usuarioId + '"';
 return this.http.get<{[key: string] : Reservacion}>(url)
 .pipe(map(dta =>{
 const rests = [];
 for(const key in dta){
 if(dta.hasOwnProperty(key)){
 rests.push(
 new Reservacion(
 key,
 dta[key].restauranteId,
 dta[key].restaurante,
 dta[key].nombre,
 dta[key].horario,
 dta[key].imgUrl,
 dta[key].usuarioId));
 }
 }
 return rests;
 }),
 tap(rest => {
 this._reservaciones.next(rest);
 }));
 }
 addReservacion(restaurante: Restaurante, nombre: string, horario: string){
 const rsv = new Reservacion(
 null,
 restaurante.id,
 restaurante.titulo,
 nombre,
 horario,
 restaurante.imgUrl,
 this.usuarioId
 );
 this.http.post<any>(environment.firebaseUrl + 'reservaciones.json', {...rsv}).subscribe(data => {
 console.log(data);
 });
 }
 removeReservacion(reservacionId: string){
 return this.http.delete(`${environment.firebaseUrl}reservaciones/${reservacionId}.json`)
 .pipe(switchMap(()=>{
 return this.reservaciones;
 }), take(1), tap(rsvs => {
 this._reservaciones.next(rsvs.filter(r => r.id !== reservacionId))
 }))
 }
 constructor(
 private http: HttpClient,
 private loginService: LoginService
 ) {
 this.loginService.usuarioId.subscribe(usuarioId => {
 this.usuarioId = usuarioId;
 });
 }

 getReservacion(reservacionId: string){
  const url = environment.firebaseUrl + `reservaciones/${reservacionId}.json`;

  return this.http.get<Reservacion>(url)
  .pipe(map(dta => {
  return new Reservacion(
  reservacionId,
  dta.restauranteId,
  dta.restaurante,
  dta.nombre,
  dta.horario,
  dta.imgUrl,
  dta.usuarioId);
  }));
  }
  updateReservacion(reservacionId: string, reservacion: Reservacion){
  const url = environment.firebaseUrl + `reservaciones/${reservacionId}.json`;
  this.http.put<any>(url, {...reservacion}).subscribe(data => {
  console.log(data);
  });
  }

}
